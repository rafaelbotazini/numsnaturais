﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumNaturais
{
    public static class Matematica
    {
        public static bool NumeroPrimo(Natural n)
        {
            // numero de divisores do número
            int divisores = 2;

            for (int i = 2; i < n.Valor / 2; i++)
            {
                if ((double)n.Valor % i == 0)
                {
                    divisores++;
                }
            }

            return divisores == 2;
        }
    }
}
