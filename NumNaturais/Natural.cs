﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumNaturais
{
    public class Natural
    {
        private int _valor = 0;

        public int Valor
        {
            get { return _valor; }
        }

        public Natural(int i)
        {
            if (i >= 0)
                _valor = i;
            else throw new ArgumentException("Números naturais devem ser maiores ou iguais a zero.");
        }
        
        public Natural Antecessor()
        {
            return new Natural(_valor == 0 ? 0 : (_valor - 1));
        }

        public Natural Sucessor()
        {
            return new Natural(_valor + 1);
        }

        #region Sobrecargas de operadores
        public static Natural operator +(Natural a, Natural b)
        {
            return new Natural(a.Valor + b.Valor);
        }

        public static Natural operator -(Natural a, Natural b)
        {
            try
            {
                return new Natural(a.Valor - b.Valor);
            }
            catch
            {
                return null;
            }
        }

        public static Natural operator *(Natural a, Natural b)
        {
            return new Natural(a.Valor * b.Valor);
        }

        public static Natural operator /(Natural a, Natural b)
        {
            if (b.Valor > 0)
            {
                //verificando se é um número natural, se não, deve retornar nulo
                bool isDecimal = (double)a.Valor % b.Valor != 0;
                
                return !isDecimal ? new Natural(a.Valor / b.Valor) : null;
            }
            else throw new Exception("Impossível dividir por 0.");
        }
        #endregion
    }
}
