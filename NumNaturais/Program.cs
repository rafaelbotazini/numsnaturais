﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumNaturais
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Write("new Natural(-1) = ");
                Natural n = new Natural(-1);
                Console.WriteLine("{0}", n.Valor);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            var n2 = new Natural(6);
            var n3 = new Natural(5);
            var n4 = new Natural(2);

            Natural n5 = n2 + n3;
            Natural n6 = n2 - n3;
            Natural n7 = n3 - n2;
            Natural n8 = n3 / n2;
            Natural n9 = n2 / new Natural(2);

            Console.WriteLine("6 + 5 = {0}", n5.Valor);
            Console.WriteLine("6 - 5 = {0}", n6.Valor);

            Console.WriteLine("5 - 6 = {0}", (n7 != null ? n7.Valor.ToString() : "Objeto nulo"));
            Console.WriteLine("6 / 5 = {0}", (n8 != null ? n8.Valor.ToString() : "Objeto nulo"));

            Console.WriteLine("6 / 2 = {0}", n9.Valor);

            Console.WriteLine("Antecessor de 6 = {0}", n2.Antecessor().Valor);
            Console.WriteLine("Sucessor de 6 = {0}", n2.Sucessor().Valor);

            var n11 = new Natural(0);

            Console.WriteLine("Antecessor de 0 = {0}", n11.Antecessor().Valor);
            Console.WriteLine("Sucessor de 0 = {0}", n11.Sucessor().Valor);

            Console.WriteLine("6 é primo = {0}", Matematica.NumeroPrimo(n2).ToString());

            var n12 = new Natural(11);

            Console.WriteLine("11 é primo = {0}", Matematica.NumeroPrimo(n12).ToString());

            Console.ReadKey();
        }
    }
}
